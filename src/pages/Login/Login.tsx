import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../../hooks";
import { Container, Typography, Box, Alert } from "@mui/material";
import { LoginForm } from "../../components";

export const Login = () => {
  const { isAuthenticated, auth } = useAuth();
  const navigate = useNavigate();
  const { state } = useLocation();
  const [error, setError] = useState("");

  useEffect(() => {
    if (isAuthenticated) {
      navigate(state || "/", { replace: true });
    }
  });

  const onSubmit = async (username: string, password: string) => {
    const { error } = await auth(username, password);

    if (error) {
      setError(error);
    }
  };

  return (
    <Container sx={{ paddingY: 5 }}>
      <Typography fontWeight={700} variant="h4" align="center">
        Login Page
      </Typography>
      <Box sx={{ margin: "30px auto", maxWidth: "400px" }}>
        {error && (
          <Alert variant="filled" severity="error" sx={{marginBottom: 4}}>
            {error}
          </Alert>
        )}
        <LoginForm onSubmit={onSubmit} />
      </Box>
    </Container>
  );
};
