import { createContext, PropsWithChildren, useState, useEffect } from "react";
import { authService, userType } from "../services/auth";

export type AuthContextType = {
  isAuthenticated: boolean;
  auth: (
    username: string,
    password: string
  ) => Promise<{ error?: string; success?: boolean }>;
  logout: () => void;
};

export const AuthContext = createContext<AuthContextType | undefined>(
  undefined
);

export const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [user, setUser] = useState<userType | null>(null);

  useEffect(() => {
    const userFromLS = localStorage.getItem("user");
    userFromLS && setUser(JSON.parse(userFromLS));
  }, []);

  const auth = async (username: string, password: string): Promise<object> => {
    return authService
      .login({ username, password })
      .then((user) => {
        localStorage.setItem("user", JSON.stringify(user));
        setUser(user);
        return { success: true };
      })
      .catch((error) => {
        return { error };
      });
  };

  const logout = () => {
    localStorage.removeItem("user");
    setUser(null);
  };

  return (
    <AuthContext.Provider value={{ isAuthenticated: !!user, auth, logout }}>
      {children}
    </AuthContext.Provider>
  );
};
