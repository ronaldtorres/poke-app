import "./App.css";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { customTheme } from "./theme";
import { AuthProvider } from "./contexts/AuthContext";
import { AppRouter } from "./router/AppRouter";


function App() {
  return (
    <div className="App">
      <ThemeProvider theme={customTheme}>
        <AuthProvider>
          <CssBaseline />
          <AppRouter />
        </AuthProvider>
      </ThemeProvider>
    </div>
  );
}

export default App;
