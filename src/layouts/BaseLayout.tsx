import { FC, PropsWithChildren } from "react";
import { Topbar } from "../components";
import { Container } from "@mui/material";

export const BaseLayout: FC<PropsWithChildren> = ({ children }) => {
  return (
    <div>
      <Topbar />
      <Container sx={{ marginTop: "100px" }}>{children}</Container>
    </div>
  );
};
