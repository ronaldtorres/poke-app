import { useNavigate, useParams } from "react-router-dom";
import { BaseLayout } from "../../layouts";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Button, Box, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { getPokemon } from "../../services/api";
import { Pokemon, pokemonTypesColors } from "../../types/Pokemon";
import { PokemonCard, PokemonStat } from "../../components";
import { useFavorites } from "../../hooks";

export const Details = () => {
  const { name } = useParams();
  const [pokemon, setPokemon] = useState<Pokemon>();
  const navigate = useNavigate();
  const { addFavorite, removeFavorite, favorites } = useFavorites();

  const handleFavorite = (pokemon: Pokemon, newStatus: boolean) => {
    newStatus ? addFavorite(pokemon) : removeFavorite(pokemon);
  };

  const [{ color }] = pokemonTypesColors.filter(
    (type) => pokemon?.types[0].type.name.indexOf(type.name) !== -1
  );

  useEffect(() => {
    // Could use redux or Context to manage de pokemon in a global store and avoid refetch.
    if (!name) return;

    getPokemon(name).then((res) => {
      if (res.data) {
        setPokemon(res.data);
      }
    });
  }, [name]);

  return !pokemon ? null : (
    <BaseLayout>
      <Box>
        <Button onClick={() => navigate(-1)}>
          <ArrowBackIcon /> Back
        </Button>
        <Grid sx={{ paddingTop: 4 }} container spacing={8}>
          <Grid item lg={4}>
            <Box sx={{ paddingTop: 3 }}>
              <PokemonCard
                pokemon={pokemon}
                isFavorite={pokemon.name in favorites}
                onClickFavorite={handleFavorite}
              />
            </Box>
          </Grid>
          <Grid item lg={8}>
            <Typography variant="h3" fontWeight={700}>
              Stats for{" "}
              <span style={{ textTransform: "capitalize", color }}>
                {pokemon.name}
              </span>
            </Typography>
            <Box sx={{ paddingBottom: 2 }}>
              {pokemon.stats.map((stat, i) => (
                <PokemonStat color={color} key={i} stat={stat} />
              ))}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </BaseLayout>
  );
};
