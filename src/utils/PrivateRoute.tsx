import React, { FC, PropsWithChildren, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../hooks";

export const PrivateRoute: FC<PropsWithChildren> = ({ children }) => {
  const { isAuthenticated } = useAuth();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  useEffect(() => {
    if (!isAuthenticated) {
      navigate("/login", { state: pathname, replace: true });
    }
  }, [isAuthenticated, navigate, pathname]);

  return <React.Fragment>{children}</React.Fragment>;
};
