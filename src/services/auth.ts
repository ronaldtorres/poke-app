export const MockUser = {
  username: "admin",
  password: "admin",
};

export type userType = {
  username: string;
};

export const authService = {
  async login(user: typeof MockUser): Promise<userType | null> {
    if (
      user.username === MockUser.username &&
      user.password === MockUser.password
    ) {
      return Promise.resolve({ username: user.username });
    }

    return Promise.reject("Wrong user or password");
  },
};
