import { Link } from "react-router-dom";
import { styled } from "@mui/system";

export const NavLink = styled(Link)<{ showlg: string }>`
  padding: ${(props) => (props.showlg === "true" ? "20px" : "8px")};
  font-size: 16px;
  text-decoration: none;
  color: #fff;
  font-weight: 600;

  &:hover {
    border-bottom: 2px solid #ccc;
  }
`;
