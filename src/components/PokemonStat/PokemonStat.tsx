import {
  Box,
  Typography,
  LinearProgress,
  linearProgressClasses,
} from "@mui/material";
import { styled } from "@mui/system";
import { FC } from "react";
import { PokemonStatType } from "../../types/Pokemon";

export const PokemonStat: FC<{ stat: PokemonStatType; color: string }> = ({
  stat,
  color,
}) => {
  return (
    <>
      <Typography variant="h6" sx={{ textTransform: "capitalize", marginY: 1 }}>
        {stat.stat.name}
      </Typography>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Typography variant="h6" sx={{ fontWeight: 700 }}>
          {stat.base_stat}
        </Typography>
        <Box sx={{ flexGrow: 1, marginLeft: 2 }}>
          <BorderLinearProgress
            bar={color}
            sx={{ width: "100%" }}
            value={stat.base_stat <= 100 ? stat.base_stat : 100}
            variant="determinate"
          />
        </Box>
      </Box>
    </>
  );
};

const BorderLinearProgress = styled(LinearProgress)<{ bar: string }>(
  ({ theme, bar }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: bar,
    },
  })
);
