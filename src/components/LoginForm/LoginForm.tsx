import { Box, TextField, Stack, Button } from "@mui/material";
import { useFormik } from "formik";
import { FC } from "react";
import * as yup from "yup";

const validationSchema = yup.object({
  username: yup.string().required("Username is required"),
  password: yup
    .string()
    .min(3, "Password should be of minimum 3 characters length")
    .required("Password is required"),
});

type LoginFormType = {
  onSubmit: (user: string, pass: string) => void;
};

export const LoginForm: FC<LoginFormType> = ({ onSubmit }) => {
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      onSubmit(values.username, values.password);
    },
  });

  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ "& .MuiTextField-root": { width: "100%" } }}
      autoComplete="off"
    >
      <Stack spacing={4}>
        <TextField
          id="username"
          name="username"
          label="Username"
          value={formik.values.username}
          onChange={formik.handleChange}
          error={formik.touched.username && Boolean(formik.errors.username)}
          helperText={formik.touched.username && formik.errors.username}
        />
        <TextField
          id="password"
          name="password"
          label="Password"
          type="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />
        <Button type="submit">Login</Button>
      </Stack>
    </Box>
  );
};
