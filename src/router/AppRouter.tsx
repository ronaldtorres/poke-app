import { Route, Routes } from "react-router-dom";
import { DetailsPage, DummyPage, HomePage, LoginPage } from "../pages";
import { PrivateRoute } from "../utils/PrivateRoute";

export const AppRouter = () => {
  return (
    <Routes>
      <Route path="login" element={<LoginPage />} />
      <Route
        path="/"
        element={
          <PrivateRoute>
            <HomePage />
          </PrivateRoute>
        }
      />
      <Route
        path="/details/:name"
        element={
          <PrivateRoute>
            <DetailsPage />
          </PrivateRoute>
        }
      />
      <Route
        path="/dummy"
        element={
          <PrivateRoute>
            <DummyPage />
          </PrivateRoute>
        }
      />
      <Route path="*" element={<div>Route not found</div>} />
    </Routes>
  );
};
