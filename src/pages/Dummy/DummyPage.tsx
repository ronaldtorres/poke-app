import { useNavigate } from "react-router-dom";
import { BaseLayout } from "../../layouts";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Button, Box } from "@mui/material";

export const DummyPage = () => {
  const navigate = useNavigate();

  return (
    <BaseLayout>
      <Box>
        <Button onClick={() => navigate(-1)}>
          <ArrowBackIcon /> Back
        </Button>
        <h1>Dummy Page</h1>
      </Box>
    </BaseLayout>
  );
};
