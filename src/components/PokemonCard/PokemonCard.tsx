import { FC } from "react";
import {
  Box,
  CardContent,
  CardMedia,
  Typography,
  IconButton,
} from "@mui/material";
import StraightenIcon from "@mui/icons-material/Straighten";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import StarIcon from "@mui/icons-material/Star";
import { Pokemon, pokemonTypesColors } from "../../types/Pokemon";
import { CardChip, CardContainer, CardTitle, CardContentBox } from "./styled";

type CardTpe = {
  pokemon: Pokemon;
  isFavorite?: boolean;
  onClickFavorite?: (p: Pokemon, state: boolean) => void;
};

export const PokemonCard: FC<CardTpe> = ({
  pokemon,
  isFavorite,
  onClickFavorite,
}) => {
  const imgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${pokemon.id}.png`;
  const [{ color }] = pokemonTypesColors.filter(
    (type) => pokemon.types[0].type.name.indexOf(type.name) !== -1
  );

  return (
    <CardContainer color={color}>
      <CardTitle>{pokemon.name}</CardTitle>
      <CardMedia
        component="img"
        image={imgUrl}
        alt={pokemon.name}
        sx={{ pointerEvents: "none", minHeight: "300px", minWidth: "300px" }}
      />
      <CardContent>
        <CardContentBox>
          <Box>
            <Typography fontWeight="bold" variant="subtitle1">
              <CardChip
                label={pokemon.types[0].type.name}
                bg={color}
                size="small"
              />
            </Typography>
            <Typography variant="body1" color="text.secondary">
              Type
            </Typography>
          </Box>
          <Box>
            <Typography fontWeight="bold" variant="subtitle1">
              {`${pokemon.weight / 10}`} kg
            </Typography>
            <Typography variant="body1" color="text.secondary">
              Weight
            </Typography>
          </Box>
          <Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <StraightenIcon
                sx={{ marginRight: 0.5, transform: "rotate(90deg)" }}
              />
              <Typography fontWeight="bold" variant="subtitle1">
                <span>{`${pokemon.height / 10}`} m</span>
              </Typography>
            </Box>
            <Typography variant="body1" color="text.secondary">
              Height
            </Typography>
          </Box>
          <Box>
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                onClickFavorite && onClickFavorite(pokemon, !isFavorite);
              }}
            >
              {isFavorite ? (
                <StarIcon fontSize="large" />
              ) : (
                <StarBorderIcon fontSize="large" />
              )}
            </IconButton>
          </Box>
        </CardContentBox>
      </CardContent>
    </CardContainer>
  );
};
