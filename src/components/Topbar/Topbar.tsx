import { AppBar, Box, Toolbar, Typography, Button } from "@mui/material";
import { useAuth } from "../../hooks";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { NavLink } from "./styled";

export const Topbar = () => {
  const { logout } = useAuth();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));

  return (
    <Box>
      <AppBar position="fixed">
        <Toolbar
          sx={{
            color: "white",
            display: "flex",
            justifyContent: "space-between",
            flexDirection: matches ? "row" : "column",
          }}
        >
          <Typography color="white" variant="h5" component="div">
            PokeApp
          </Typography>
          <div>
            <NavLink to="/" showlg={matches.toString()}>
              Home
            </NavLink>
            <NavLink to="/dummy" showlg={matches.toString()}>
              Dummy
            </NavLink>
            <span>|</span>
            <Button onClick={() => logout()} sx={{ marginLeft: 2 }}>
              Logout
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
