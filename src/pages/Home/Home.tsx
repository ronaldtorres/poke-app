import { BaseLayout } from "../../layouts";
import { Box, IconButton, Fade, Grid } from "@mui/material";
import { PokemonCard } from "../../components";
import { useFavorites, usePokemonList, useScroll } from "../../hooks";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import { Pokemon } from "../../types/Pokemon";

export const Home = () => {
  const { pokemonList, loadMore } = usePokemonList();
  const [scroll, setScroll] = useScroll();
  const { addFavorite, removeFavorite, favorites } = useFavorites();

  const handleFavorite = (pokemon: Pokemon, newStatus: boolean) => {
    newStatus ? addFavorite(pokemon) : removeFavorite(pokemon);
  };

  useEffect(() => {
    if (scroll.percentage >= 95) {
      setTimeout(() => {
        loadMore();
        setScroll((prev) => ({ ...prev, percentage: 5 }));
      }, 100);
    }
  }, [scroll, loadMore, setScroll]);

  const goToTop = () => {
    document.body.scrollIntoView();
  };

  return (
    <BaseLayout>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={8}>
          {pokemonList.map((pokemon, i) => (
            <Grid key={i + pokemon.name + pokemon.id} item md={6} lg={4}>
              <Link
                to={`/details/${pokemon.name}`}
                style={{ textDecoration: "none" }}
              >
                <PokemonCard
                  pokemon={pokemon}
                  isFavorite={pokemon.name in favorites}
                  onClickFavorite={handleFavorite}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Fade in={scroll.percentage >= 5}>
        <IconButton
          onClick={goToTop}
          sx={{
            position: "fixed",
            right: 10,
            bottom: 10,
            backgroundColor: "primary.dark",
          }}
        >
          <ExpandLessIcon />
        </IconButton>
      </Fade>
    </BaseLayout>
  );
};
